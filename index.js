const units = ['нула', 'едно', 'две', 'три', 'четири', 'пет', 'шест', 'седем', 'осем', 'девет'];
const tensFromTenToTwenty = ['десет', 'единадесет', 'дванадесет', 'тринадесет', 'четиринадесет', 'петнадесет',
  'шестнадесет', 'седемнадесет', 'осемнадесет', 'деветнадесет'];
const tens = ['двадесет', 'тридесет', 'четиридесет', 'петдесет', 'шестдесет', 'седемдесет', 'осемдесет', 'деветдесет'];
const hundreds = ['сто', 'двеста', 'триста', 'четиристотин', 'петстотин', 'шестстотин', 'седемстотин', 'осемстотин', 'деветстотин'];
const thousands = ['хиляда'].concat(units.slice(2).map(unit => unit + ' хиляди'));

function replaceNumbers(text) {
  const numbers = text.match(/\d+/g).map(n => parseInt(n));
  numbers.forEach(number => {
    const indexInText = text.indexOf(number);
    if (indexInText > 0 && indexInText < text.length - 1) {
      if (!isLetter(text[indexInText - 1]) && !isLetter(text[indexInText + 1])) {
        text = text.replace(number, getNumberAsText(number));
      }
    } else if (indexInText > 0 && !isLetter(text[indexInText - 1])) {
      text = text.replace(number, getNumberAsText(number));
    } else if (indexInText < text.length - 1 && !isLetter(text[indexInText + 1])) {
      text = text.replace(number, getNumberAsText(number));
    }
  });
  return text;
}

function isLetter(character) {
  return character >= 'а' || character >= 'я' || character >= 'А' || character >= 'Я'
}

function getNumberAsText(number) {
  if (number < 1000) {
    return getNumberUpToThousandAsText(number);
  }

  const firstDigit = parseInt(number / 1000);
  const numberUpToThousand = number % 1000;

  let numberAsText = thousands[firstDigit - 1];
  if (numberUpToThousand !== 0) {
    const numberUpToThousandAsText = getNumberUpToThousandAsText(number % 1000);
    if (numberUpToThousandAsText.indexOf(' и ') !== -1) {
      numberAsText += ' ' + numberUpToThousandAsText;
    } else {
      numberAsText += ' и ' + numberUpToThousandAsText;
    }
  }
  return numberAsText;
}

function getNumberUpToThousandAsText(number) {
  const firstDigit = parseInt(number / 100) % 10;
  const secondDigit = parseInt(number / 10) % 10;
  const thirdDigit = number % 10;

  let numberAsText = '';
  if (number >= 100) {
    numberAsText += hundreds[firstDigit - 1];

    if (secondDigit > 1) {
      if (thirdDigit !== 0) {
        numberAsText += ' ' + tens[secondDigit - 2] + ' и ' + units[thirdDigit];
      } else {
        numberAsText += ' и ' + tens[secondDigit - 2];
      }
    } else if (secondDigit === 1) {
      numberAsText += ' и ' + tensFromTenToTwenty[thirdDigit];
    } else {
      if (thirdDigit !== 0) {
        numberAsText += ' и ' + units[thirdDigit];
      }
    }
  } else {
    if (secondDigit > 1) {
      if (thirdDigit !== 0) {
        numberAsText += tens[secondDigit - 2] + ' и ' + units[thirdDigit];
      } else {
        numberAsText += tens[secondDigit - 2];
      }
    } else if (secondDigit === 1) {
      numberAsText += tensFromTenToTwenty[thirdDigit];
    } else {
      numberAsText += units[thirdDigit];
    }
  }

  return numberAsText;
}

console.log(replaceNumbers('магистрала А1 456 друг път Б123 655 5'));
